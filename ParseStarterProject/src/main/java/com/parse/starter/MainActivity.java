/*
 * Copyright (c) 2015-present, Parse, LLC.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */
package com.parse.starter;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;

import java.util.List;


public class MainActivity extends AppCompatActivity implements View.OnClickListener, View.OnKeyListener {

    Boolean signUpModeActive = true;

    TextView changeSignupModeTextView;

    EditText passwordEditText;

    EditText usernameEditText;

    RelativeLayout relativeLayout;

    ImageView logoImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        passwordEditText = (EditText) findViewById(R.id.passwordEditText);

        usernameEditText = (EditText) findViewById(R.id.usernameEditText);

        changeSignupModeTextView = (TextView) findViewById(R.id.changeSignupModeTextView);

        relativeLayout = (RelativeLayout) findViewById(R.id.backgroundRelativeLayout);

        logoImageView = (ImageView) findViewById(R.id.logoImageView);

        setTitle("Instagram");



        changeSignupModeTextView.setOnClickListener(this);

        passwordEditText.setOnKeyListener(this);

        relativeLayout.setOnClickListener(this);

        logoImageView.setOnClickListener(this);
        
        //Already Logged in
        if(ParseUser.getCurrentUser() != null){

            showUserList();

        }

        ParseAnalytics.trackAppOpenedInBackground(getIntent());
    }


    public  void showUserList(){
        Intent intent = new Intent(getApplicationContext(), UsersListActivity.class);
        startActivity(intent);
    }

    //DETECTS ANY KEYPRESSED ON THE KEYBOARD, HAD TO ADD AT THE CLASS BLOCK View.OnKeyListener
    //CHECK onCreate() METHOD
    @Override
    public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {

        //NEED TO IMPLEMENT && keyEvent.getAction() == keyEvent.ACTION_DOWN
        //OTHERWISE onKey() GETS CALLED TWICE, ONCE WHEN YOU PRESS ON SOMETHING AND IMMEDIATELY WHEN YOU LIFT YOUR FINGER
        //THE ENTER HERE IS FOR AFTER THE USERNAME AND PASSWORD WAS PUT IN

        if(keyCode == keyEvent.KEYCODE_ENTER  && keyEvent.getAction() == keyEvent.ACTION_DOWN){
            signUp(view);
        }
        return false;
    }


    //GIVES TEXTVIEW onClick FUNCTIONALITY, HAD TO ADD View.OnClickListener IN THE CLASS
    //DETECT A PRESS/CLICK ANYWHERE ON THE SCREEN, to hide keyboard
    //ADD AN ID TO THE Layout, in this case RelativeLayout
    //ADD AN ID TO THE LOGO/IMAGEVIEW
    //CHECK onCreate() FOR MORE
    //CHECK onCreate() METHOD
    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.changeSignupModeTextView) {

            Button signupButton = (Button) findViewById(R.id.signupButton);

            if (signUpModeActive) {

                signUpModeActive = false;
                signupButton.setText("Login");
                changeSignupModeTextView.setText("Or, Signup");

            } else {

                signUpModeActive = true;
                signupButton.setText("Signup");
                changeSignupModeTextView.setText("Or, Login");

            }

        }

        else if(view.getId() == R.id.backgroundRelativeLayout || view.getId() == R.id.logoImageView){

            //THIS GETS THE KEYBOARD FOR US
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);


        }

    }

    public void signUp(View view) {


        if (usernameEditText.getText().toString().matches("") || passwordEditText.getText().toString().matches("")) {

            Toast.makeText(this, "A username and password are required.", Toast.LENGTH_SHORT).show();

        } else {

            if (signUpModeActive) {

                ParseUser user = new ParseUser();

                user.setUsername(usernameEditText.getText().toString());
                user.setPassword(passwordEditText.getText().toString());

                user.signUpInBackground(new SignUpCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e == null) {

                            showUserList();
                            Log.i("Signup", "Successful");

                        } else {

                            Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    }
                });

            } else {

                ParseUser.logInInBackground(usernameEditText.getText().toString(), passwordEditText.getText().toString(), new LogInCallback() {
                    @Override
                    public void done(ParseUser user, ParseException e) {

                        if (user != null) {

                            showUserList();
                            Log.i("Signup", "Login successful");

                        } else {

                            Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();

                        }


                    }
                });


            }
        }


    }


}
