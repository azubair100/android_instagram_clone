package com.parse.starter;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseAnonymousUtils;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class UsersListActivity extends AppCompatActivity {


    public void getPhoto() {
        //Choose image from the device

        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, 1);

    }

    //Permission for taking photo
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 1) {

            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                getPhoto();

            }


        }

    }

    //Menu code
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();

        menuInflater.inflate(R.menu.share_menu, menu);

        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == R.id.share){
            //Check if we are in Marshmellow
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);

                    //After permission has been granted go to line 27

                } else {
    //permission already granted
                    getPhoto();

                }
            }
            //If we are not in marsh mellow
            else{

                getPhoto();

            }
        }
        else if(item.getItemId() == R.id.log_out){

            ParseUser.logOut();

            //Jump back to main actiivty

            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);

        }

        return super.onOptionsItemSelected(item);
    }


    //THIS METHOD UPLOADS THE IMAGES TO THE PARSER
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1 && resultCode == RESULT_OK && data != null) {

            //This gets the image from storage
            Uri selectedImage = data.getData();

            try {

                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);

                Log.i("Photo", "Recieved");



                //THIS IS THE CODE THAT UPLOADS THE IMAGE TO THE PARSE SERVER

                //this line will help us transform the bitmap into a parse file so we can upload it on to the parse server
                //To get a parse file you need to go through byteArray
                ByteArrayOutputStream stream = new ByteArrayOutputStream();

                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);

                byte[] byteArray = stream.toByteArray();

                ParseFile file = new ParseFile("image.png", byteArray);

                //this is where we are gonna store all our images
                ParseObject object = new ParseObject("Image");

                //columns in this object, image file, username,
                object.put("image", file);

                object.put("username", ParseUser.getCurrentUser().getUsername());

                object.saveInBackground(
                        new SaveCallback() {
                            @Override
                            public void done(ParseException e) {

                                if(e == null){
                                    Toast.makeText(UsersListActivity.this, "Image Shared", Toast.LENGTH_LONG).show();
                                }

                                else{
                                    Toast.makeText(UsersListActivity.this, "Image could not be shared, please try again later", Toast.LENGTH_LONG).show();
                                }
                            }
                        }
                );

                //END OF THE SECTION


                // Add this line to the manifesto
                // <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />
                //The request uers permission at onCreate()

            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users_list);

        setTitle("User Feed");
        //this how you get the users 32-69


        final ArrayList<String> usernames = new ArrayList<String>();

        final ListView userListView = (ListView) findViewById(R.id.user_list);


        userListView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        //this will go to the user feed activity
                        Intent intent = new Intent(getApplicationContext(), UserFeedActivity.class);

                        //which user was clicked
                        intent.putExtra("username", usernames.get(position));

                        startActivity(intent);

                    }
                }
        );

        final ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, usernames);

        ParseQuery<ParseUser> query = ParseUser.getQuery();

        query.whereNotEqualTo("username", ParseUser.getCurrentUser().getUsername());

        query.addAscendingOrder("username");

        query.findInBackground(new FindCallback<ParseUser>() {
            @Override
            public void done(List<ParseUser> objects, ParseException e) {

                if (e == null) {

                    if (objects.size() > 0) {

                        for (ParseUser user : objects) {

                            usernames.add(user.getUsername());

                        }

                        userListView.setAdapter(arrayAdapter);
                    }

                } else {

                    e.printStackTrace();

                }

            }
        });










        ParseAnalytics.trackAppOpenedInBackground(getIntent());


    }


}
